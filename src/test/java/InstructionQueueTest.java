import instruction.queue.InstructionMessage;
import instruction.queue.InstructionQueue;
import instruction.queue.InstructionType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static org.junit.jupiter.api.Assertions.*;

class InstructionQueueTest {

    InstructionMessage instructionMessage;
    InstructionMessage incorrectInstructionMessage;
    InstructionQueue instructionQueue;
    
    final String INSTRUCTION_MESSAGE_STR = "InstructionMessage";
    final InstructionType INSTRUCTION_TYPE_A = InstructionType.A;
    final String PRODUCT_CODE = "AB12";
    final int QUANTITY = 25;
    final int UOM = 128;
    final String TIMESTAMP = "2023-05-30T09:30:55.123";
    
    @BeforeEach
    public void setup() {
        instructionMessage = new InstructionMessage(INSTRUCTION_MESSAGE_STR, INSTRUCTION_TYPE_A, PRODUCT_CODE, QUANTITY, UOM, TIMESTAMP);
        instructionQueue = new InstructionQueue();
    }

    @Test
    void getterCorrectlyGetsInstructionMessageStr() {
        assertEquals(INSTRUCTION_MESSAGE_STR, instructionMessage.getInstructionMessageStr());
    }

    @Test
    void getterCorrectlyGetsInstructionType() {
        assertEquals(INSTRUCTION_TYPE_A, instructionMessage.getInstructionType());
    }

    @Test
    void getterCorrectlyGetsProductCode() {
        assertEquals(PRODUCT_CODE, instructionMessage.getProductCode());
    }

    @Test
    void getterCorrectlyGetsQuantity() {
        assertEquals(QUANTITY, instructionMessage.getQuantity());
    }

    @Test
    void getterCorrectlyGetsUom() {
        assertEquals(UOM, instructionMessage.getUom());
    }

    @Test
    void getterCorrectlyGetsTimestamp() {
        assertEquals(TIMESTAMP, instructionMessage.getTimestamp());
    }
    
    @Test
    void enqueueCorrectlyQueuesInstructionMessage() {
        instructionQueue.enqueue(instructionMessage);
        List<InstructionMessage> instructionQueueComparison = new ArrayList<>();
        instructionQueueComparison.add(instructionMessage);
        assertEquals(instructionQueueComparison.get(0), instructionQueue.getFirstInstructionMessage());
    }
    
    @Test
    void instructionMessageStrThrowsExceptionWhenMisspelled() {
        String incorrectInstructionMessageStr = "InsturctionMessage";

        incorrectInstructionMessage = new InstructionMessageTestBuilder().withInstructionMessageStr(incorrectInstructionMessageStr).build();

        assertThrows(IllegalArgumentException.class, () -> instructionQueue.enqueue(incorrectInstructionMessage));
    }

    @Test
    void productCodeThrowsExceptionWhenTooManyCharacters() {
        String incorrectProductCode = "AB123";

        incorrectInstructionMessage = new InstructionMessageTestBuilder().withProductCode(incorrectProductCode).build();

        assertThrows(IllegalArgumentException.class, () -> instructionQueue.enqueue(incorrectInstructionMessage));
    }

    @Test
    void productCodeThrowsExceptionWhenNotEnoughCharacters() {
        String incorrectProductCode = "A12";

        incorrectInstructionMessage = new InstructionMessageTestBuilder().withProductCode(incorrectProductCode).build();

        assertThrows(IllegalArgumentException.class, () -> instructionQueue.enqueue(incorrectInstructionMessage));
    }

    @Test
    void productCodeThrowsExceptionWhenTooManyLetters() {
        String incorrectProductCode = "ABC1";

        incorrectInstructionMessage = new InstructionMessageTestBuilder().withProductCode(incorrectProductCode).build();

        assertThrows(IllegalArgumentException.class, () -> instructionQueue.enqueue(incorrectInstructionMessage));
    }

    @Test
    void quantityThrowsExceptionWhenZero() {
        int incorrectQuantity = 0;

        incorrectInstructionMessage = new InstructionMessageTestBuilder().withQuantity(incorrectQuantity).build();

        assertThrows(IllegalArgumentException.class, () -> instructionQueue.enqueue(incorrectInstructionMessage));
    }

    @Test
    void uomThrowsExceptionWhenBelowZero() {
        int incorrectUom = -1;

        incorrectInstructionMessage = new InstructionMessageTestBuilder().withUom(incorrectUom).build();

        assertThrows(IllegalArgumentException.class, () -> instructionQueue.enqueue(incorrectInstructionMessage));
    }

    @Test
    void uomThrowsExceptionWhenAboveMaximumValue() {
        int incorrectUom = 256;

        incorrectInstructionMessage = new InstructionMessageTestBuilder().withUom(incorrectUom).build();

        assertThrows(IllegalArgumentException.class, () -> instructionQueue.enqueue(incorrectInstructionMessage));
    }

    @Test
    void timestampThrowsExceptionWhenIncorrectLetterUsed() {
        String incorrectTimestamp = "2023-05-30G09:30:55.123";

        incorrectInstructionMessage = new InstructionMessageTestBuilder().withTimestamp(incorrectTimestamp).build();

        assertThrows(IllegalArgumentException.class, () -> instructionQueue.enqueue(incorrectInstructionMessage));
    }

    @Test
    void timestampThrowsExceptionWhenNoMilliseconds() {
        String incorrectTimestamp = "2023-05-30T09:30:55";

        incorrectInstructionMessage = new InstructionMessageTestBuilder().withTimestamp(incorrectTimestamp).build();

        assertThrows(IllegalArgumentException.class, () -> instructionQueue.enqueue(incorrectInstructionMessage));
    }

    @Test
    void timestampThrowsExceptionWhenBeforeUnixEpoch() {
        String incorrectTimestamp = "1969-12-31T23:59:59.999";

        incorrectInstructionMessage = new InstructionMessageTestBuilder().withTimestamp(incorrectTimestamp).build();

        assertThrows(IllegalArgumentException.class, () -> instructionQueue.enqueue(incorrectInstructionMessage));
    }

    @Test
    void timestampThrowsExceptionWhenAfterNow() {
        String incorrectTimestamp = "2056-05-30T09:30:55.123";

        incorrectInstructionMessage = new InstructionMessageTestBuilder().withTimestamp(incorrectTimestamp).build();

        assertThrows(IllegalArgumentException.class, () -> instructionQueue.enqueue(incorrectInstructionMessage));
    }

    @Test
    void timestampThrowsExceptionWhenImpossibleDateUsed() {
        String incorrectTimestamp = "2023-02-29T09:30:55.123";

        incorrectInstructionMessage = new InstructionMessageTestBuilder().withTimestamp(incorrectTimestamp).build();

        assertThrows(IllegalArgumentException.class, () -> instructionQueue.enqueue(incorrectInstructionMessage));
    }

    @Test
    void timestampThrowsExceptionWhenImpossibleTimeUsed() {
        String incorrectTimestamp = "2023-02-28T24:30:55.123";

        incorrectInstructionMessage = new InstructionMessageTestBuilder().withTimestamp(incorrectTimestamp).build();

        assertThrows(IllegalArgumentException.class, () -> instructionQueue.enqueue(incorrectInstructionMessage));
    }

    @Test
    void peekCorrectlyReturnsHighestPriorityMessage() {
        InstructionMessage instructionMessageLowerPriority = new InstructionMessageTestBuilder().withInstructionType(InstructionType.B).build();

        instructionQueue.enqueue(instructionMessage);
        instructionQueue.enqueue(instructionMessageLowerPriority);
        assertEquals(instructionMessage, instructionQueue.peek());

        InstructionQueue instructionQueue1 = new InstructionQueue();
        instructionQueue1.enqueue(instructionMessageLowerPriority);
        instructionQueue1.enqueue(instructionMessage);
        assertEquals(instructionMessage, instructionQueue1.peek());
    }
    
    @Test
    void receiveCorrectlyReceivesMessageAndEnqueuesIt() {
        String instructionMessageAsString = INSTRUCTION_MESSAGE_STR + " " + INSTRUCTION_TYPE_A + " " + PRODUCT_CODE + " " + QUANTITY + " " + UOM + " " + TIMESTAMP;
        
        instructionQueue.receive(instructionMessageAsString);
    
        assertEquals(instructionMessage, instructionQueue.peek());
    }
    
    @Test
    void receiveThrowsExceptionWhenTooManyArguments() {
        String instructionMessageAsStringTooManyArgs = INSTRUCTION_MESSAGE_STR + " " + INSTRUCTION_TYPE_A + " " + PRODUCT_CODE + " " + QUANTITY + " " + UOM + " " + TIMESTAMP + " ExtraStuff";
    
        assertThrows(IllegalArgumentException.class, () -> instructionQueue.receive(instructionMessageAsStringTooManyArgs));
    }
    
    @Test
    void receiveThrowsExceptionWhenNotEnoughArguments() {
        String instructionMessageAsStringNotEnoughArgs = INSTRUCTION_MESSAGE_STR + " " + INSTRUCTION_TYPE_A + " " + PRODUCT_CODE + " " + QUANTITY + " " + UOM;
    
        assertThrows(IllegalArgumentException.class, () -> instructionQueue.receive(instructionMessageAsStringNotEnoughArgs));
    }
    
    @Test
    void dequeueCorrectlyReturnsHighestPriorityMessage() {
        InstructionMessage instructionMessageLowerPriority = new InstructionMessageTestBuilder().withInstructionType(InstructionType.B).build();
    
        instructionQueue.enqueue(instructionMessage);
        instructionQueue.enqueue(instructionMessageLowerPriority);
        assertEquals(instructionMessage, instructionQueue.dequeue());
    
        InstructionQueue instructionQueue1 = new InstructionQueue();
        instructionQueue1.enqueue(instructionMessageLowerPriority);
        instructionQueue1.enqueue(instructionMessage);
        assertEquals(instructionMessage, instructionQueue1.dequeue());
    }
    
    @Test
    void dequeueCorrectlyRemovesHighestPriorityMessageFromQueue() {
        InstructionMessage instructionMessageLowerPriority = new InstructionMessageTestBuilder().withInstructionType(InstructionType.B).build();
    
        instructionQueue.enqueue(instructionMessage);
        instructionQueue.enqueue(instructionMessageLowerPriority);
        instructionQueue.dequeue();
        assertEquals(instructionMessageLowerPriority, instructionQueue.peek());
    
        InstructionQueue instructionQueue1 = new InstructionQueue();
        instructionQueue1.enqueue(instructionMessageLowerPriority);
        instructionQueue1.enqueue(instructionMessage);
        instructionQueue1.dequeue();
        assertEquals(instructionMessageLowerPriority, instructionQueue1.peek());
    }
    
    @Test
    void countCorrectlyCountsNumberOfInstructionMessagesInQueue() {
        assertEquals(0, instructionQueue.count());
    
        instructionQueue.enqueue(instructionMessage);
        assertEquals(1, instructionQueue.count());
    }
    
    @Test
    void isEmptyCorrectlyReturnsTrueWhenQueueIsEmpty() {
        assertTrue(instructionQueue.isEmpty());
    }
    
    @Test
    void isEmptyCorrectlyReturnsFalseWhenQueueIsNotEmpty() {
        instructionQueue.enqueue(instructionMessage);
        assertFalse(instructionQueue.isEmpty());
    }
}