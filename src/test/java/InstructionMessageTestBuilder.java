import instruction.queue.InstructionMessage;
import instruction.queue.InstructionType;

public class InstructionMessageTestBuilder {

    private String instructionMessageStr = "InstructionMessage";
    private InstructionType instructionType = InstructionType.A;
    private String productCode = "AB12";
    private int quantity = 150;
    private int uom = 25;
    private String timestamp = "2023-05-12T09:30:00.123";

    public InstructionMessageTestBuilder withInstructionMessageStr(String instructionMessageStr) {
        this.instructionMessageStr = instructionMessageStr;
        return this;
    }

    public InstructionMessageTestBuilder withInstructionType(InstructionType instructionType) {
        this.instructionType = instructionType;
        return this;
    }

    public InstructionMessageTestBuilder withProductCode(String productCode) {
        this.productCode = productCode;
        return this;
    }

    public InstructionMessageTestBuilder withQuantity(int quantity) {
        this.quantity = quantity;
        return this;
    }

    public InstructionMessageTestBuilder withUom(int uom) {
        this.uom = uom;
        return this;
    }

    public InstructionMessageTestBuilder withTimestamp(String timestamp) {
        this.timestamp = timestamp;
        return this;
    }

    public InstructionMessage build() {
        return new InstructionMessage(instructionMessageStr, instructionType, productCode, quantity, uom, timestamp);
    }
}
