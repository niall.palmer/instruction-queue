package instruction.queue;

import instruction.queue.validators.*;

import java.util.*;

public class InstructionQueue implements MessageReceiver {
    private static final int INSTRUCTION_MESSAGE_SIZE = 6;
    private final PriorityQueue<InstructionMessage> instructionQueue = new PriorityQueue<>();
    private final List<Validator> validators = new ArrayList<>(5);
    public void applyValidators() {
        validators.add(new InstructionMessageStrValidator());
        validators.add(new ProductCodeValidator());
        validators.add(new QuantityValidator());
        validators.add(new UomValidator());
        validators.add(new TimestampValidator());
    }

    public void enqueue(InstructionMessage instructionMessage) {
        applyValidators();

        for (Validator validator : validators) {
            validator.checkValidation(instructionMessage);
        }

        instructionQueue.add(instructionMessage);
    }

    public InstructionMessage getFirstInstructionMessage() {
        return instructionQueue.peek();
    }

    @Override
    public void receive(String instructionMessageAsString) {
        List<String> instructionMessageAsList = Arrays.asList(instructionMessageAsString.split(" "));
        
        if (instructionMessageAsList.size() != INSTRUCTION_MESSAGE_SIZE)
            throw new IllegalArgumentException("The instruction message is the incorrect number of arguments");
        
        InstructionMessage instructionMessage = new InstructionMessage(
                instructionMessageAsList.get(0),
                InstructionType.valueOf(instructionMessageAsList.get(1)),
                instructionMessageAsList.get(2),
                Integer.parseInt(instructionMessageAsList.get(3)),
                Integer.parseInt(instructionMessageAsList.get(4)),
                instructionMessageAsList.get(5)
        );
        enqueue(instructionMessage);
    }
    
    public InstructionMessage peek() {
        return instructionQueue.peek();
    }
    
    public InstructionMessage dequeue() {
        return instructionQueue.poll();
    }
    
    public int count() {
        return instructionQueue.size();
    }
    
    public boolean isEmpty() {
        return instructionQueue.isEmpty();
    }
}
