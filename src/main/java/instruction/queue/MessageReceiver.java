package instruction.queue;

public interface MessageReceiver {
    void receive(String instructionMessageAsString);
}
