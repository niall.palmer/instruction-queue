package instruction.queue.validators;

import instruction.queue.InstructionMessage;

public class UomValidator implements Validator {

    public static final int MINIMUM_UOM = 0;
    public static final int MAXIMUM_UOM = 255;

    @Override
    public void checkValidation(InstructionMessage instructionMessage) {
        if (instructionMessage.getUom() < MINIMUM_UOM || instructionMessage.getUom() > MAXIMUM_UOM) {
            throw new IllegalArgumentException("The UOM must be between 0 and 256 inclusive");
        }
    }
}
