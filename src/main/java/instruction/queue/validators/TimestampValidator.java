package instruction.queue.validators;

import instruction.queue.InstructionMessage;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.time.format.ResolverStyle;

public class TimestampValidator implements Validator {

    public static final LocalDateTime UNIX_EPOCH = LocalDateTime.of(1970, 1, 1, 0, 0, 0, 0);

    @Override
    public void checkValidation(InstructionMessage instructionMessage) {
        LocalDateTime timestamp;
        LocalDateTime currentDateTime = LocalDateTime.now();
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("uuuu-MM-dd'T'HH:mm:ss.SSS").withResolverStyle(ResolverStyle.STRICT);

        try {
            timestamp = LocalDateTime.parse(instructionMessage.getTimestamp(), dateTimeFormatter);
        } catch (DateTimeParseException exception) {
            throwException();
        }

        timestamp = LocalDateTime.parse(instructionMessage.getTimestamp(), dateTimeFormatter);

        if (timestamp.isBefore(UNIX_EPOCH)) {
            throwException();
        } else if (timestamp.isAfter(currentDateTime)) {
            throwException();
        }

    }

    public void throwException() {
        throw new IllegalArgumentException("The timestamp must be in the format of \"yyyy-MM-dd'T'HH:mm:ss.SSS\"; it must be before now; it must be after Unix Epoch");
    }
}
