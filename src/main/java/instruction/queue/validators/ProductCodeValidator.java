package instruction.queue.validators;

import instruction.queue.InstructionMessage;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ProductCodeValidator implements Validator {

    @Override
    public void checkValidation(InstructionMessage instructionMessage) {
        String productCode = instructionMessage.getProductCode();

        Pattern pattern = Pattern.compile("[A-Z]{2}[0-9]{2}");
        Matcher matcher = pattern.matcher(productCode);

        if (!matcher.matches()) {
            throw new IllegalArgumentException("The product code must have exactly four characters - two uppercase letters follows by two digits");
        }
    }
}
