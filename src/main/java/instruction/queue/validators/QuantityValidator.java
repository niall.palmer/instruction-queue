package instruction.queue.validators;

import instruction.queue.InstructionMessage;

public class QuantityValidator implements Validator {

    public static final int MINIMUM_QUANTITY = 1;

    @Override
    public void checkValidation(InstructionMessage instructionMessage) {
        if (instructionMessage.getQuantity() < MINIMUM_QUANTITY) {
            throw new IllegalArgumentException("The quantity must not be below 1");
        }
    }
}
