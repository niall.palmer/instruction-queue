package instruction.queue;

import java.util.Objects;

public class InstructionMessage implements Comparable<InstructionMessage> {

    private final String instructionMessageStr;
    private final InstructionType instructionType;
    private final String productCode;
    private final int quantity;
    private final int uom;
    private final String timestamp;
    
    public InstructionMessage(String instructionMessageStr, InstructionType instructionType, String productCode, int quantity, int uom, String timestamp) {
        this.instructionMessageStr = instructionMessageStr;
        this.instructionType = instructionType;
        this.productCode = productCode;
        this.quantity = quantity;
        this.uom = uom;
        this.timestamp = timestamp;
    }
    
    public String getInstructionMessageStr() {
        return instructionMessageStr;
    }
    
    public InstructionType getInstructionType() {
        return instructionType;
    }
    
    public String getProductCode() {
        return productCode;
    }
    
    public int getQuantity() {
        return quantity;
    }
    
    public int getUom() {
        return uom;
    }
    
    public String getTimestamp() {
        return timestamp;
    }
    
    @Override
    public int compareTo(InstructionMessage instructionMessage) {
        return Integer.compare(this.getInstructionType().getPriority(), instructionMessage.getInstructionType().getPriority());
    }
    
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        InstructionMessage that = (InstructionMessage) o;
        return quantity == that.quantity && uom == that.uom && Objects.equals(instructionMessageStr, that.instructionMessageStr) && instructionType == that.instructionType && Objects.equals(productCode, that.productCode) && Objects.equals(timestamp, that.timestamp);
    }
    
    @Override
    public int hashCode() {
        return Objects.hash(instructionMessageStr, instructionType, productCode, quantity, uom, timestamp);
    }
}
